from tkinter import *
from tkinter import ttk
from smtplib import SMTP
from email.message import EmailMessage


def main(*args):
    smtp_server = str(tk_server_entry.get())
    sender_address = str(tk_sender_entry.get())
    password = str(tk_password_entry.get())
    recipient_address = str(tk_recipient_entry.get())
    smtp_port = int(tk_port_entry.get())
    use_ssl = bool(tk_use_ssl.get())


    attempt_message(smtp_server, sender_address, password, recipient_address, smtp_port, use_ssl)


def attempt_message(server, sender, password, recipient, port, ssl):
    if ssl is True:
        message = EmailMessage()

        message['Subject'] = "Test message from SMTP test tool"
        message['From'] = sender
        message['To'] = recipient

        remote_server = SMTP(host=server, port=port)
        remote_server.connect(host=server, port=port)
        remote_server.ehlo()
        remote_server.starttls()
        remote_server.ehlo()
        remote_server.login(user=sender, password=password)
        remote_server.send_message(msg=message)
        remote_server.quit()

    else:
        message = EmailMessage()
        message.set_content("This is a test message from blah blah blah")

        message['Subject'] = "Test message from SMTP test tool"
        message['From'] = sender
        message['To'] = recipient

        remote_server = SMTP(host=server, port=port)
        remote_server.connect(host=server, port=port)
        remote_server.login(user=sender, password=password)
        remote_server.send_message(msg=message)
        remote_server.quit()


root = Tk()
root.title("SMTP Test Utility")

mainframe = ttk.Frame(root, padding="10 10 21 21")
mainframe.grid(column=0, row=0)
root.columnconfigure(0, weight=100)
root.rowconfigure(0, weight=100)

tk_server = StringVar()
tk_server_entry = ttk.Entry(mainframe, width=20, textvariable=tk_server)
tk_server_entry.grid(column=24, row=20)
tk_server_entry.insert(0,"smtp.office365.com")
ttk.Label(mainframe, text="SMTP server address: ").grid(column=20, row=20)

tk_sender_email = StringVar()
tk_sender_entry = ttk.Entry(mainframe, width=20, textvariable=tk_sender_email, exportselection=False)
tk_sender_entry.grid(column=24, row=30, )
ttk.Label(mainframe, text="Sender address\\Username: ").grid(column=20, row=30)

tk_password = StringVar()
tk_password_entry = ttk.Entry(mainframe, width=20, textvariable=tk_password, show="*", exportselection=False)
tk_password_entry.grid(column=24, row=40)
ttk.Label(mainframe, text="Password: ").grid(column=20, row=40)


tk_recipient_email = StringVar()
tk_recipient_entry = ttk.Entry(mainframe, width=20, textvariable=tk_recipient_email, exportselection=False)
tk_recipient_entry.grid(column=24, row=50)
ttk.Label(mainframe, text="Recipient address: ").grid(column=20, row=50)

tk_port = StringVar()
tk_port_entry = ttk.Entry(mainframe, width=5, textvariable=tk_port, exportselection=False)
tk_port_entry.grid(column=24, row=60)
tk_port_entry.insert(0, "587")
ttk.Label(mainframe, text="SMTP Port: ").grid(column=20, row=60)

tk_use_ssl = BooleanVar()
tk_ssl_entry = ttk.Checkbutton(mainframe, text="Use SSL", variable=tk_use_ssl, onvalue=1, offvalue=0)
tk_ssl_entry.grid(column=24, row=70)

ttk.Button(mainframe, text="Send Message", command=main).grid(column=30, row=100, sticky=(S, W))

for child in mainframe.winfo_children():
    child.grid_configure(padx=5, pady=5)
tk_server_entry.focus()

root.mainloop()
